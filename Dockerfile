# Extending image
FROM node:13.6.0

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get -y install autoconf automake libtool nasm make pkg-config git apt-utils

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Versions
RUN npm -v
RUN node -v

# Install app dependencies
COPY package.json /usr/src/app/
COPY package-lock.json /usr/src/app/

RUN yarn install

# Bundle app source
COPY . /usr/src/app

# Port to listener
EXPOSE 9000

# Environment variables
ENV NODE_ENV production
ENV PORT 9000
ENV PUBLIC_PATH "/"

RUN yarn build

# Main command
CMD [ "yarn", "serve" ]
