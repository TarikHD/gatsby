import React from "react"
import Layout from "../components/layout"
import { Link, graphql } from "gatsby"
import PropTypes from "prop-types"

const Article = ({ data }) => {
  const articles = data.nodeArticle

  return (
    <Layout>
      <Link to={articles.path.alias}>
        <h1>{articles.title}</h1>
        <div dangerouslySetInnerHTML={{ __html: articles.body.processed }} />
      </Link>
    </Layout>
  )
}

Article.protoTypes = { data: PropTypes.object.isRequired }

export const query = graphql`
  query($ArticleId: String!) {
    nodeArticle(id: { eq: $ArticleId }) {
      id
      title
      body {
        processed
      }
      path {
        alias
      }
    }
  }
`
export default Article
