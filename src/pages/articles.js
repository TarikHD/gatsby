import React from "react"
import Layout from "../components/layout"
import { graphql, Link } from "gatsby"

export const query = graphql`
  query {
    allNodeArticle {
      nodes {
        id
        title
        body {
          processed
        }
        path {
          alias
        }
      }
    }
  }
`

// fetchData()
const ArticlesPage = ({ data }) => {
  const Articlespage = data.allNodeArticle.nodes

  return (
    <Layout>
      {Articlespage.map((article, id) => {
        return (
          <Link to={article.path.alias} key={id}>
            <h1>{article.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: article.body.processed }}></div>
          </Link>
        )
      })}
    </Layout>
  )
}
export default ArticlesPage
