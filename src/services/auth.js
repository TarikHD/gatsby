import axios from "axios"
import { navigate } from "gatsby"

export const isBrowser = () => typeof window !== "undefined"
export const getUser = () =>
  isBrowser() && window.localStorage.getItem("gatsbyUser")
    ? JSON.parse(window.localStorage.getItem("gatsbyUser"))
    : {}
const setUser = user =>
  window.localStorage.setItem("gatsbyUser", JSON.stringify(user))

export const login = (login, password) => {
  if (
    localStorage.getItem("access_token") !== null &&
    localStorage.getItem("access_token") !== "undefined"
  ) {
    fetchToken(localStorage.getItem("access_token"))
  } else {
    axios
      .post("http://localhost:8084/user/login?_format=json", {
        name: login,
        pass: password,
      })
      .then(data => {
        fetchToken(data.data.access_token, data.data.current_user.name)
      })
  }
}

export const fetchToken = (token, username) => {
  axios
    .get("http://localhost:8084/jwt/token", {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/hal+json",
        Accept: "application/hal+json",
      },
    })
    .then(data => {
      if (data.data.token) {
        // fetchArticles(data.data.token)
        localStorage.setItem("access_token", data.data.token)

        navigate(`/app/profile`)

        return setUser({
          name: username,
          token: data.data.token,
        })
      }
    })
}

export const handleLogin = ({ username, password }) => {
  login(username, password)
}

export const isLoggedIn = () => {
  const user = getUser()
  return !!user.name
}

export const logout = callback => {
  setUser({})
  localStorage.removeItem("access_token")

  callback()
}
